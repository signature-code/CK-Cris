using CK.Cris;

namespace CK.Auth;

/// <summary>
/// Removes any current authentication information.
/// </summary>
public interface ILogoutCommand : ICommand
{
}
