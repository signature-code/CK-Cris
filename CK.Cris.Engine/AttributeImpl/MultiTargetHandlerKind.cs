namespace CK.Setup.Cris;

enum MultiTargetHandlerKind
{
    RoutedEventHandler,
    IncomingValidator,
    ConfigureAmbientServices,
    CommandHandlingValidator,
    RestoreAmbientServices,
}
