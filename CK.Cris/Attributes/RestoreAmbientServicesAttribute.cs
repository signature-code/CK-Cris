using CK.Core;
using CK.Setup;
using System;
using System.Runtime.CompilerServices;

namespace CK.Cris;

/// <summary>
/// Decorates a method that can configure <see cref="AmbientServiceHub"/> from a command or a command part.
/// The method is called in a background context before any [CommandHandlingValidator] and the [CommandHandler] method.
/// It must be implemented on a singleton and accepts only singleton parameters with the exception of the
/// scoped <see cref="IActivityMonitor"/> (that is optional).
/// </summary>
[AttributeUsage( AttributeTargets.Method, AllowMultiple = false, Inherited = false )]
public sealed class RestoreAmbientServicesAttribute : ContextBoundDelegationAttribute
{
    /// <summary>
    /// Initializes a new <see cref="ConfigureAmbientServicesAttribute"/>.
    /// </summary>
    /// <param name="fileName">Captures the source file name of the validator definition.</param>
    /// <param name="lineNumber">Captures the source line number of the validator definition.</param>
    public RestoreAmbientServicesAttribute( [CallerFilePath] string? fileName = null, [CallerLineNumber] int lineNumber = 0 )
        : base( "CK.Setup.Cris.RestoreAmbientServicesAttributeImpl, CK.Cris.Engine" )
    {
        FileName = fileName;
        LineNumber = lineNumber;
    }

    /// <summary>
    /// Gets the file name that defines this handler.
    /// </summary>
    public string? FileName { get; }

    /// <summary>
    /// Gets the line number that defines this handler.
    /// </summary>
    public int LineNumber { get; }
}
